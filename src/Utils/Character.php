<?php

namespace WorldOfWarcraftAPI\Utils;

use WorldOfWarcraftAPI\Client;
use WorldOfWarcraftAPI\Utils\Guild;
use WorldOfWarcraftAPI\Utils\CharacterStats;

class Character extends ClientResponse
{
    /**
     * ...
     *
     * @var string
     */
    protected $thumbnail = 'http://render-api-%1$s.worldofwarcraft.com/static-render/%1$s/%2$s';

    /**
     * ...
     *
     * @var WorldOfWarcraftAPI\Utils\CharacterStats|null
     */
    protected $stats = null;

    /**
     * ...
     *
     * @var WorldOfWarcraftAPI\Utils\Guild|null
     */
    protected $guild = null;

    /**
     * ...
     *
     * @var integer
     */
    protected $guildRank = -1;

    /**
     * ...
     *
     * @param WorldOfWarcraftAPI\Client $instance
     * @param array                     $data
     */
    public function __construct(Client $instance, array $data)
    {
        parent::__construct($instance, $data);
    }

    /**
     * ...
     *
     * @param  string|null $region
     * @return string
     */
    public function thumbnail($region = null)
    {
        $region = $region ?: $this->instance->settings()->get('client.region');

        return sprintf($this->thumbnail, $region, $this->data['thumbnail']);
    }

    /**
     * ...
     *
     * @return WorldOfWarcraftAPI\Utils\CharacterStats
     */
    public function stats()
    {
        return $this->stats = $this->stats != null ?
            $this->stats : new CharacterStats($this->instance, $this->data->get('stats'));
    }

    /**
     * ...
     *
     * @param  boolean $format
     * @param  null    $title
     * @return string|null
     */
    public function title($format = true, $title = null)
    {
        foreach ($this->data->get('titles') as $title) {
            if (isset($title['selected'])) {
                break;
            }
        }

        return $format ? sprintf($title['name'], $this->data->get('name')) : $title;
    }

    /**
     * ...
     *
     * @return WorldOfWarcraftAPI\Utils\Guild
     */
    public function guild()
    {
        $guild = $this->data->get('guild');

        return $this->guild = $this->guild ?: $this->instance->getGuild($guild['name'], $guild['realm']);
    }

    /**
     * ...
     *
     * @param int $rank
     */
    public function setGuildRank($rank)
    {
        $this->guildRank = $rank;
    }

    /**
     * ...
     *
     * @param WorldOfWarcraftAPI\Utils\Guild $guild
     */
    public function setGuild(Guild $guild)
    {
        $this->guild = $guild;
    }
}
