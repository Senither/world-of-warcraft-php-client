<?php

namespace WorldOfWarcraftAPI\Utils;

use WorldOfWarcraftAPI\Client;

class CharacterStats extends ClientResponse
{
    /**
     * ...
     *
     * @param WorldOfWarcraftAPI\Client $instance
     * @param array                     $data
     */
    public function __construct(Client $instance, array $data)
    {
        parent::__construct($instance, $data);
    }
}
