<?php

namespace WorldOfWarcraftAPI\Utils;

use WorldOfWarcraftAPI\Client;
use Illuminate\Support\Collection;
use WorldOfWarcraftAPI\Exceptions\UnknownPropertyException;

abstract class ClientResponse
{
    /**
     * ...
     *
     * @var WorldOfWarcraftAPI\Client
     */
    protected $instance;

    /**
     * ...
     *
     * @var Illuminate\Support\Collection
     */
    protected $data;

    /**
     * ...
     *
     * @param WorldOfWarcraftAPI\Client $instance
     * @param array                     $data
     */
    public function __construct(Client $instance, array $data)
    {
        $this->instance = $instance;
        $this->data     = Collection::make($data);
    }

    /**
     * ...
     *
     * @param  string $name
     * @param  array  $args
     * @return mixed
     */
    public function __call($name, $args)
    {
        if (!isset($this->data[$name])) {
            throw new UnknownPropertyException("An attempt was made to access an unknown property with the name of '{$name}'");
        }

        $data = $this->data[$name];

        return $this->isReplaceableProperty($data) ?
            $this->getReplacedProperty($data, !empty($args) ? $args[0] : true) : $data;
    }

    /**
     * ...
     *
     * @param  string  $name
     * @return boolean
     */
    public function has($name)
    {
        return $this->data->has($name);
    }

    /**
     * ...
     *
     * @return Illuminate\Support\Collection
     */
    public function all()
    {
        return $this->data;
    }

    /**
     * ...
     *
     * @param  array   $value
     * @param  boolean $pretty
     * @return integer|string
     */
    protected function getReplacedProperty(array $value, $pretty)
    {
        return $pretty ? $value['name'] : $value['id'];
    }

    /**
     * ...
     *
     * @param  string $property
     * @return boolean
     */
    protected function isReplaceableProperty($property)
    {
        return is_array($property) && count($property) == 2 && isset($property['id']) && isset($property['name']);
    }
}
