<?php

namespace WorldOfWarcraftAPI;

use WoWClient;
use Illuminate\Cache\FileStore;
use WorldOfWarcraftAPI\Utils\Guild;
use Illuminate\Filesystem\Filesystem;
use WorldOfWarcraftAPI\Utils\Character;

class Client
{
    use Traits\ClientSettingsTrait,
        Traits\ClientResourceTrait;

    /**
     *
     *
     * @var Illuminate\Cache\FileStore
     */
    private $cache;

    /**
     * Creates a new world of warcraft client instance.
     *
     * @param array       $settings
     * @param string|null $settingsPath
     */
    public function __construct(array $settings = [], $settingsPath = null)
    {
        $this->cache = new FileStore(new Filesystem, __DIR__ . '/../cache');

        // Loads the settings file
        $this->loadSettings($settings, $settingsPath);
        $this->loadResources();
    }

    /**
     * ...
     *
     * @param  string $name
     * @param  string $realm
     * @return WorldOfWarcraftAPI\Utils\Character
     */
    public function getCharacter($name, $realm)
    {
        $response = $this->cache->get(sprintf('characters.%s.%s', $realm, $name));

        if ($response == null) {
            $response = $this->sendRequest('character', [$realm, $name], ['feed', 'guild', 'professions', 'progression', 'pvp', 'stats', 'titles', 'appearance', 'items'], true);

            $response = $this->replaceValue('class', $response, 'characterClasses');
            $response = $this->replaceValue('race', $response, 'characterRaces');
            $response['gender'] = [
                'id'   => $response['gender'],
                'name' => ($response['gender'] == 0) ? 'Male' : 'Female'
            ];

            $this->cache->put(sprintf('characters.%s.%s', $realm, $name), $response, $this->settings->get('cache.time'));
        }

        return new Character($this, $response);
    }

    /**
     * ...
     *
     * @param  string $name
     * @param  string $realm
     * @return WorldOfWarcraftAPI\Utils\Guild
     */
    public function getGuild($name, $realm)
    {
        $response = $this->cache->get(sprintf('guild.%s.%s', $realm, $name));

        if ($response == null) {
            $response = $this->sendRequest('guild', [$realm, $name], ['members', 'news', 'challenge', 'achievements'], true);

            $members = $response['members'];

            foreach ($members as $index => $member) {
                $member = $member['character'];
                $member = $this->replaceValue('class', $member, 'characterClasses');
                $member = $this->replaceValue('race', $member, 'characterRaces');
                $member['gender'] = [
                    'id'   => $member['gender'],
                    'name' => ($member['gender'] == 0) ? 'Male' : 'Female'
                ];

                $response['members'][$index]['character'] = $member;
            }

            $this->cache->put(sprintf('guild.%s.%s', $realm, $name), $response, $this->settings->get('cache.time'));
        }

        return new Guild($this, $response);
    }

   /**
    * ...
    *
    * @param  string $name
    * @param  array  $obj
    * @param  string $res
    * @return
    */
    private function replaceValue($name, $obj, $res)
    {
        $value = $obj[$name];

        // Loads the defined resource and loops throught the content.
        foreach ($this->loadResource($res) as $k => $a) {
            foreach ($a as $i => $v) {
                if ($v['id'] == $value) {
                    $obj[$name] = [
                        'id'   => $v['id'],
                        'name' => $v['name']
                    ];

                    return $obj;
                }
            }
        }

        return $obj;
    }

   /**
    * ...
    *
    * @param  string  $func
    * @param  array   $param
    * @param  array   $selector
    * @param  boolean $trim
    * @return string|false
    */
    public function sendRequest($func, $param, $selector = null, $trim = false)
    {
        $region    = $this->settings->get('client.region', 'eu');
        $local     = $this->settings->get('client.local', 'en_GB');
        $publicKey = $this->settings->get('keys.public');

        // Generates the request call URL.
        $url = "https://{$region}.api.battle.net/wow/{$func}/";
        foreach ($param as $key) {
            $url .= rawurlencode($key) . '/';
        }

        // Removes the '/' character at the end of the url if trim is set to true.
        $url = ($trim) ? trim($url, '/') : $url;

        // Generates the selectors if there is any.
        if ($selector != null && is_array($selector)) {
            $selectors = '';
            foreach ($selector as $key) {
                $selectors .= $key . ',';
            }
            $url .= '?fields=' . trim($selectors, ',');
        }

        $url .= "&locale={$local}&apikey={$publicKey}";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);
        $header = curl_getinfo($ch);

        curl_close($ch);

        if ($header['http_code'] != 200) {
            return false;
        }

        return json_decode($output, true);
    }
}
