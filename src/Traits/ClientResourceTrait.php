<?php

namespace WorldOfWarcraftAPI\Traits;

trait ClientResourceTrait
{
    /**
     * An array of loaded resources, used for array cashing
     * so we can access loaded resources faster.
     *
     * @var array
     */
    protected $loadedResources = [];

    /**
     * ...
     *
     * @var array
     */
    protected $resources = [
        'battlegroups/'         => 'battlegroups',
        'character/races'       => 'characterRaces',
        'character/classes'     => 'characterClasses',
        'character/achievements'=> 'characterAchievements',
        'guild/rewards'         => 'guildRewards',
        'guild/perks'           => 'guildPerks',
        'guild/achievements'    => 'guildAchievements',
        'item/classes'          => 'itemClasses',
        'talents'               => 'talents',
        'pet/types'             => 'petTypes'
    ];

    /**
     * ...
     *
     * @return array
     */
    public function loadResources()
    {
        foreach ($this->resources as $url => $name) {
            if ($this->cache->get('resources.' . $name) == null) {
                if (($resource = $this->getResource($url)) !== false) {
                    $this->cache->forever('resources.' . $name, json_decode($resource, true));
                }
            }

            $this->resource[] = $name;
        }
    }

    /**
     * ...
     *
     * @throws \RuntimeException
     *
     * @param  string $name
     * @return string
     */
    public function loadResource($name)
    {
        if (isset($this->loadedResources[$name])) {
            return $this->loadedResources[$name];
        }

        if (!in_array($name, $this->resources)) {
            throw new RuntimeException("Invalid resource name given!");
        }

        $source = $this->cache->get('resources.' . $name) ?: [];

        $this->loadedResources[$name] = $source;

        return $source;
    }

    /**
     * ...
     *
     * @param  string $name
     * @return string|boolean
     */
    private function getResource($name)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "http://{$this->settings->get('client.region')}.battle.net/api/wow/data/{$name}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);
        $header = curl_getinfo($ch);

        curl_close($ch);

        if ($header['http_code'] != 200) {
            return false;
        }

        return $output;
    }
}
