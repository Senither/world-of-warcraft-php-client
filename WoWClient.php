<?php

use WorldOfWarcraftAPI\Client;

class WoWClient
{
    /**
     * The instance of our World of Warcraft Client.
     * 
     * @var mixed
     */
    private static $instance = null;

    /**
     * Calls a given method on instance staticly.
     * 
     * @param  string $name 
     * @param  array  $args 
     * @return mixed       
     */
    public static function __callStatic($name, $args)
    {
        if (self::$instance == null) {
            self::$instance = new Client;
        }
        
        return call_user_func_array([self::$instance, $name], $args);
    }
}
