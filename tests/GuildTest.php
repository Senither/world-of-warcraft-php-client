<?php 

class GuildTest extends TestCase
{
    private $guilds = [];

    public function test_a_guild_call_returns_a_guild_object()
    {
        $guild = $this->getGuild('Serenity', 'Twisting Nether');

        $this->assertInstanceOf($guild, $this->buildUtils('Guild'));
    }

    public function test_a_guild_object_has_guild_data()
    {
        $guild = $this->getGuild('Serenity', 'Twisting Nether');

        $this->assertEquals($guild->name(),  'Serenity');
        $this->assertEquals($guild->realm(), 'Twisting Nether');

        $this->assertArrayHasKeys($guild->all(), [
            'lastModified', 'members', 'news', 'challenge'
        ]);
    }

    public function test_getting_a_member_from_the_guild_object_will_return_a_character_object()
    {
        $guild = $this->getGuild('Serenity', 'Twisting Nether');

        $this->assertInstanceOf($guild->members()->random(), $this->buildUtils('Character'));
    }

    protected function getGuild($name, $realm)
    {
        if (isset($this->guilds[$realm]) && isset($this->guilds[$realm][$name])) {
            return $this->guilds[$realm][$name];
        }

        return $this->guilds[$realm][$name] = $this->client->getGuild($name, $realm);
    }
}
