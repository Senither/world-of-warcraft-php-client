<?php

namespace WorldOfWarcraftAPI;

class Settings
{
    /**
     * ...
     *
     * @var array
     */
    protected $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    public function get($name, $default = null)
    {
        $parsed = explode('.', $name);
        if (!is_array($parsed) || empty($parsed)) {
            return $default;
        }

        $setting = $this->settings;

        foreach ($parsed as $key) {
            if (isset($setting[$key])) {
                $setting = $setting[$key];
            } else {
                return $default;
            }
        }

        return $setting;
    }

    public function all()
    {
        return $this->settings;
    }
}
