<?php

namespace WorldOfWarcraftAPI\Traits;

use \WorldOfWarcraftAPI\Settings;

trait ClientSettingsTrait
{
    /**
     * ...
     *
     * @var WorldOfWarcraftAPI\Settings
     */
    protected $settings;

    /**
     * ...
     *
     * @param  array $settings
     * @param  string|null $path
     * @return void
     */
    public function loadSettings($settings, $path = null)
    {
        $conf = require $path == null ? __DIR__ . '/../../settings.conf' : $path;

        $this->settings = new Settings(array_merge($settings, $conf));
    }

    /**
     * ...
     *
     * @return WorldOfWarcraftAPI\Settings
     */
    public function settings()
    {
        return $this->settings;
    }
}
