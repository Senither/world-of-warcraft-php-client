<?php 

class TestCase extends PHPUnit_Framework_TestCase
{
    protected $client;

    public function __construct()
    {
        $this->client = new \WorldOfWarcraftAPI\Client;
    }

    protected function buildUtils($instance, $data = [])
    {
        $instance = '\\WorldOfWarcraftAPI\\Utils\\' . $instance;
        
        return new $instance($this->client, $data);
    }

    public static function assertInstanceOf($obj, $type)
    {
        self::assertTrue($obj instanceof $type);
    }

    public static function assertArrayHasKeys($array, $keys)
    {
        foreach ($keys as $key) {
            self::assertArrayHasKey($key, $array);
        }
    }
}
