<?php

namespace WorldOfWarcraftAPI\Utils;

use WorldOfWarcraftAPI\Client;
use Illuminate\Support\Collection;

class Guild extends ClientResponse
{
    /**
     * ...
     *
     * @var Illuminate\Support\Collection|null
     */
    private $members = null;

    /**
     * ...
     *
     * @param WorldOfWarcraftAPI\Client $instance
     * @param array                     $data
     */
    public function __construct(Client $instance, array $data)
    {
        parent::__construct($instance, $data);
    }

    /**
     * ...
     *
     * @return Illuminate\Support\Collection
     */
    public function members()
    {
        if ($this->members != null) {
            return $this->members;
        }

        $this->members = Collection::make();
        $guild = ['guild' => [
            'lastModified' => $this->data->get('lastModified'),
            'name'         => $this->data->get('name'),
            'realm'        => $this->data->get('realm'),
            'battlegroup'  => $this->data->get('battlegroup'),
            'level'        => $this->data->get('level')
        ]];

        foreach ($this->data->get('members') as $id => $member) {
            $char = new Character($this->instance, array_merge($member['character'], $guild));
            $char->setGuildRank(intval($member['rank']));

            $this->members->push($char);
        }

        return $this->members;
    }
}
