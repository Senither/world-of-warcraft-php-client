<?php 

class CharacterTest extends TestCase
{
    private $characters = [];

    public function test_a_character_call_returns_a_character_object()
    {
        $character = $this->getCharacter('Ríseneth', 'Kazzak');

        $this->assertInstanceOf($character, $this->buildUtils('Character'));
    }

    public function test_a_character_has_character_data()
    {
        $character = $this->getCharacter('Ríseneth', 'Kazzak');

        $this->assertEquals($character->name(),  'Ríseneth');
        $this->assertEquals($character->realm(), 'Kazzak');

        $this->assertArrayHasKeys($character->all(), [
            'lastModified', 'battlegroup', 'class', 'race', 'gender',
            'level', 'achievementPoints', 'thumbnail', 'faction', 'guild',
            'feed', 'items', 'stats', 'professions', 'titles', 'progression'
        ]);
    }

    public function test_calling_the_character_sub_methods_will_generate_the_propper_class_instances()
    {
        $character = $this->getCharacter('Ríseneth', 'Kazzak');

        $this->assertInstanceOf($character->guild(), $this->buildUtils('Guild'));
        $this->assertInstanceOf($character->stats(), $this->buildUtils('CharacterStats'));
    }

    public function test_thumbnail_are_generated_correctly()
    {
        $character = $this->getCharacter('Ríseneth', 'Kazzak');
        
        $this->assertStringStartsWith(
            'http://render-api-eu.worldofwarcraft.com/static-render/eu/kazzak/',
            $character->thumbnail()
        );
    }

    protected function getCharacter($name, $realm)
    {
        if (isset($this->characters[$realm]) && isset($this->characters[$realm][$name])) {
            return $this->characters[$realm][$name];
        }

        return $this->characters[$realm][$name] = $this->client->getCharacter($name, $realm);
    }
}
